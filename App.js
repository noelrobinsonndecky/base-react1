// Ma variable n
let n= 0

// Ma fonction render
function render(){
    const monTitre= React.createElement("h1", {}, "Bonjour Noel c'est React", "Bonjour Jean  ",
        React.createElement("span", {},  n)
    );
/**
 * h1: Tag
 * {}: ce sont les options
 * "Bonjour Noel c'est React": représente l'élément enfant de mon tag h1.
 */
    console.log(monTitre);

    ReactDOM.render(
    monTitre,
    document.querySelector("#app")
    );
}

render()

window.setInterval(() =>{
    n++
    render()
}, 1000)
