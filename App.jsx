// Ma variable n
let n= 0;

function formatNombre(n){
    return n.toString().padStart(2, "0")
}
// Ma fonction App
function App(){

    // maListe
    const maListe= [
        "1er élément",
        "2éme élément",
        "3éme élément"
    ]
    
    // Je parcours tous les éléments de ma liste

    const secondTitre = 
        <React.Fragment>
            <h1 id="monTitre">
                Bonjour Noel c'est react <span>{formatNombre(n)}</span> 
            </h1>

            <ul>
                {maListe.map((maListe, k) => <li key={k}> {maListe} </li>)}
            </ul>
        </React.Fragment>

    ReactDOM.render(
        secondTitre,
        document.querySelector("#app1")
    )
}

App();

window.setInterval(() => {
    n++
    App()
}, 1000)